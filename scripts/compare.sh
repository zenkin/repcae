#!/bin/sh

# Конфигурационный файл загружается, если еще не загружен
if [ -z "$REPCAE_PATH" ] 
    then
    . "`dirname "$0"`/../config"
    fi


# Файл для построения графа
HTML_FILE="$REPCAE_TEMP/repcae_compared.html"

"$REPCAE_PATH/scripts/compare.py" $@ > "$HTML_FILE"

nohup firefox "$HTML_FILE" >/dev/null 2>/dev/null &
