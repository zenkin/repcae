# Скрипт обрабатывает имеющиеся в репозитории компараторы.
# Результат скрипта не затрагивает репозиторий, а только его представление в фузеки
# 
# Псевдокод
# 
# запрос: ?количественная_особенность :связана_с_компаратором ?компаратор
#         ?объект ?количественная_особенность ?число
# 
# Для каждого уникального результата запроса:
#       case (?компаратор)
#           ищем настройку соответствующего компаратора в ?объект
#                 если нет: ?предок = ?объект :аффилирован ?предок
#                             ищем настройку компаратора в свойствах ?предок
#                                 если нет: ?предок = ?предок :аффилирован ?предок
#                                 цикл ввверх
#           нашли настройку компаратора ?масштабы
#           название(?количественная_особенность), ?число, ?масштабы ->
#                                                   название_новой_особ
#               (в идеале функция выше должна где-то лежать отдельными скриптами для каждого компаратора для удобства дополнения и редактирования)
#           добавление в фузеки: ?объект :имеет_особенность название_новой_особ


# Конфигурационный файл загружается, если еще не загружен
if [ -z "$REPCAE_PATH" ] 
    then
    . "`dirname "$0"`/../config"
    fi


# Временный файл для хранения результатов выдачи БЗ
TEMP_FILE="$REPCAE_TEMP/repcae_onload.ttl"

# Шапка
echo '@prefix : <http://piston-engines.ru/ontologies/cae#> .' > "$TEMP_FILE"
echo '@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .' >> "$TEMP_FILE"
echo '@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .' >> "$TEMP_FILE"




# простой_компаратор

"$REPCAE_PATH/scripts/comparators/простой_компаратор.py" | tee -a "$TEMP_FILE"

#| sed 's/http:\/\/piston-engines.ru\/ontologies\/cae\#/:/g'







# Загрузка временного файла проводится 1 раз - после обработки всех имеющихся компараторов.
$FUSEKI_PATH/bin/s-post http://localhost:3030/cae default "$TEMP_FILE"
# Удаление временного файла, чтобы не мешался
rm -v "$TEMP_FILE" 

exit




$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "PREFIX : <http://piston-engines.ru/ontologies/cae#> SELECT ?объект ?количественная_особенность ?число ?компаратор WHERE { ?количественная_особенность :обрабатовать_компаратором ?компаратор . ?объект ?количественная_особенность ?число }"

# Такая запись делает возврат всех включений, опускаясь по дереву аффилирования, но можно взять первое вхождение
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "PREFIX : <http://piston-engines.ru/ontologies/cae#> SELECT ?масштаб WHERE { :Сетка3 (:афиллирован*/:относительный_диаметр) ?масштаб }"

# Попробуем сварганить объединенный запрос

$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "PREFIX : <http://piston-engines.ru/ontologies/cae#> SELECT ?объект ?количественная_особенность ?число ?компаратор ?масштаб WHERE { {?количественная_особенность :обрабатовать_компаратором ?компаратор . ?объект ?количественная_особенность ?число} . {?объект (:афиллирован*/:относительный_диаметр) ?масштаб} }"
