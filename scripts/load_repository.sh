#!/bin/sh

# Загрузка в фузеки онтологии и пользовательских данных


# Конфигурационный файл загружается, если еще не загружен
if [ -z "$REPCAE_PATH" ] 
    then
    . "`dirname "$0"`/../config"
    fi




# Загрузка онтологии
# Больше не нужна, так как загружается при пуске сервера
# $FUSEKI_PATH/bin/s-post http://localhost:3030/cae default "$REPCAE_PATH/repCAE.ttl" && echo `date` 'Ontology is loaded from file repCAE.ttl'

# Загрузка пользовательских данных
echo `date` 'Begin to load user files...'
for FILES in `ls "$REPCAE_WORK"`
    do 
    $FUSEKI_PATH/bin/s-post http://localhost:3030/cae default "$REPCAE_WORK/$FILES" && echo `date` $FILES 'is loaded'
    done
echo `date` 'Its all.'
