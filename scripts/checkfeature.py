#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Скрипт ищет пары расчетов, такие что они как можно ближе друг к другу, 
# но при этом один содержит заданную особенность, а другой - нет
# Особенность передается через командную строку. Двоеточие добавляется динамически

import subprocess
import sys
import json
import os

FUSEKI_PATH = os.environ["FUSEKI_PATH"]

if len(sys.argv)<2:
    print 'Необходимо передать название рассматриваемой особенности в командной строке!'
    exit()

# Наименование анализируемой особенности
test_feature = sys.argv[1]
if (test_feature[0]!=':') & (test_feature[0]!='<'): 
    test_feature=':'+test_feature

print 'Анализ особенности '+test_feature


# Перечень расчетов, содержащих особенность
p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
                     PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                     SELECT DISTINCT *
                     WHERE {?s  rdf:type :Расчет. ?s :имеет_особенность '''+ test_feature +'''}
                     " ''', shell=True, stdout=subprocess.PIPE)
rez = json.load(p.stdout)

base_with=[]
# Сборка данных для каждого расчета в базу
for x in rez['results']['bindings'] :
    rec={}
    rec['name']=x['s']['value'].split('#')[1].encode('utf-8')
    # Запрос для каждого расчета списка его особенностей
    p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
                     PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                     SELECT DISTINCT ?f
                     WHERE { :'''+rec['name']+''' :имеет_особенность ?f }
                     " ''', shell=True, stdout=subprocess.PIPE)
    feat = json.load(p.stdout)
    # Сборка их в нужную форму
    rec['features']=[]
    for f in feat['results']['bindings'] :
        rec['features'].append(f['f']['value'].split('#')[1].encode('utf-8'))

    base_with.append(rec)






# Перечень расчетов, не содержащих особенности
p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
                     PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                     SELECT DISTINCT *
                     WHERE { {?s  rdf:type :Расчет} MINUS {?s  rdf:type :Расчет. ?s :имеет_особенность '''+ test_feature +'''} }
                     " ''', shell=True, stdout=subprocess.PIPE)
rez = json.load(p.stdout)

base_without=[]
# Сборка данных для каждого расчета в базу
for x in rez['results']['bindings'] :
    rec={}
    rec['name']=x['s']['value'].split('#')[1].encode('utf-8')
    # Запрос для каждого расчета списка его особенностей
    p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
                     PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                     SELECT DISTINCT ?f
                     WHERE { :'''+rec['name']+''' :имеет_особенность ?f }
                     " ''', shell=True, stdout=subprocess.PIPE)
    feat = json.load(p.stdout)
    # Сборка их в нужную форму
    rec['features']=[]
    for f in feat['results']['bindings'] :
        rec['features'].append(f['f']['value'].split('#')[1].encode('utf-8'))

    base_without.append(rec)






rezult=[]

# Для каждых пары из обеих баз
for calc_w in base_with:
    for calc_wo in base_without:
        # Находим множества уникальных особенностей
        only1=[]
        only2=list(calc_wo['features'])
        for fi in calc_w['features']:
            founded=False
            for fj in only2:
                if fi==fj: 
                    founded=True
                    only2.remove(fj)
                    break
            if not founded: only1.append(fi)
        # близость
        b = len(only1)+len(only2)
        rezult.append( (b, calc_w["name"],calc_wo["name"]) )
            
rezult.sort()
print '--------------------------------------------------------------'
print 'Близость   \tРасчет с особенностью \tРасчет без особенности'
print '--------------------------------------------------------------'
for i in range(min(len(rezult),int(os.environ["MAX_REZ"]))):
    print 'Близость '+str(rezult[i][0])+' : \t'+str(rezult[i][1])+'        \t'+str(rezult[i][2])
print '--------------------------------------------------------------'
if len(rezult)>int(os.environ["MAX_REZ"]):
    print '     Показано '+str(int(os.environ["MAX_REZ"]))+' ближайших пар из '+str(len(rezult))
