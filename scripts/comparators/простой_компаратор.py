#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import subprocess
import sys
import json
import os

FUSEKI_PATH = os.environ["FUSEKI_PATH"]

comparator_name = ':простой_компаратор'

# Функция для тестирования запроса, заданного в query. Выполняет его и делает текстовый вывод в консоль. В штатном режиме работы скрипта не используется.
def test_query():
    p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=text "
                        PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                        SELECT *'''+query+' " ', shell=True)

# Основная функция генерации особенностей. Получает все объекты, удовлетворяющие query и для каждого из них генерирует новый триплет, используя фунции генерации нового субъекта gen_subj и объекта gen_feat.
def make():
    # Инициируем запрос
    p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
                        PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                        SELECT *'''+query+' " ', shell=True, stdout=subprocess.PIPE)
    rez = json.load(p.stdout)
    # print rez
    # Для всех результатов запроса
    for x in rez['results']['bindings'] :
        if x['o']['value'].isdecimal: sys.stderr.write('Warning! Не рекомендуется использовать простой_компаратор для количественных величин. Посмотрите в сторону округляющий_до_целых_компаратор и т.п.\n')
        print ( gen_subj(x) + 
            ' :имеет_особенность ' + 
            gen_feat(x)+' .'
            )  #.keys()"
    
# ---------------------------------------------------

# Первая группа обработки: именованные объекты

query='''
WHERE {
?p :связан_с_компаратором :простой_компаратор .
?s ?p ?o .
FILTER(isURI(?s))
}
'''

# Субъект сохраняется без изменений
def gen_subj(x):
    return '<'+x['s']['value'].encode('utf-8')+'>'

# При необходимости у объекта обрезается суффикс
def strip_obj(x):
    if x['o']['type']=='literal': 
        return x['o']['value'].encode('utf-8')
    elif x['o']['type']=='uri': 
        return x['o']['value'].split('#')[1].encode('utf-8')
    else:
        return 'ERROR'

# Новое имя особенности: к старому предикату приписано его значение.
def gen_feat(x):
    return ('<'+x['p']['value'].encode('utf-8') +
           '_' +
           strip_obj(x)+'>')

# Инициируется генерация особенностей по первой группе обработки
make()


# ---------------------------------------------------

# Вторая группа обработки: безымянные объекты связанные предикатом Содержит с объектом и имеющие заданный тип

query='''
WHERE {
?p :связан_с_компаратором :простой_компаратор .
?s ?p ?o .
?s a ?types .
?par :содержит ?s .
FILTER(isBlank(?s)&&isURI(?par)&&?types!=':Геометрический_элемент')
}
'''

# В этом случае привязка идет к родителю
def gen_subj(x):
    return '<'+x['par']['value'].encode('utf-8')+'>'

# А особенность содержит дополнительно тип безымянного объекта
def gen_feat(x):
    return ('<'+x['types']['value'].encode('utf-8')+
            '_' +
            x['p']['value'].split('#')[1].encode('utf-8') +
           '_' +
           strip_obj(x)+'>')

# Инициируется генерация особенностей по второй группе обработки
make()


# ---------------------------------------------------

# Третья группа обработки - для анонимного ГУ

query='''
WHERE {
?p :связан_с_компаратором :простой_компаратор .
?s ?p ?o .
?calc :с_граничными_условиями ?s .
OPTIONAL {?s :находится_в ?place} .
FILTER(isBlank(?s)&&isURI(?calc))
}
'''

# Привязка идет для расчета, к которому относитя ГУ
def gen_subj(x):
    return '<'+x['calc']['value'].encode('utf-8')+'>'

# Особенность включает место расположения ГУ, если оно задано, предикат и объект.
def gen_feat(x):
    if 'place' in x: pl=x['place']['value'].split('#')[1].encode('utf-8')
    else: pl=''
    return (':c_ГУ_'+pl+
            '_' +
            x['p']['value'].split('#')[1].encode('utf-8') +
           '_' +
           strip_obj(x))


make()

# ---------------------------------------------------

# Четвертая группа обработки - для анонимного НУ
# Отличается от предыдущего только ключем, что это НУ.

query='''
WHERE {
?p :связан_с_компаратором :простой_компаратор .
?s ?p ?o .
?calc :с_начальными_условиями ?s .
OPTIONAL {?s :находится_в ?place} .
FILTER(isBlank(?s)&&isURI(?calc))
}
'''

def gen_subj(x):
    return '<'+x['calc']['value'].encode('utf-8')+'>'

def gen_feat(x):
    if 'place' in x: pl=x['place']['value'].split('#')[1].encode('utf-8')
    else: pl=''
    return (':c_НУ_'+pl+
            '_' +
            x['p']['value'].split('#')[1].encode('utf-8') +
           '_' +
           strip_obj(x))


make()
