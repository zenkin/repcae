#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import subprocess
import sys
import json
import os

FUSEKI_PATH = os.environ["FUSEKI_PATH"]

comparator_name = ':относительный_компаратор'

def test_query():
    p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=text "
                        PREFIX : <http://piston-engines.ru/ontologies/cae#>
                        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                        SELECT *'''+query+' " ', shell=True)


def make():
    # Инициируем запрос
    p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
                        PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                        SELECT *'''+query+' " ', shell=True, stdout=subprocess.PIPE)
    rez = json.load(p.stdout)
    # print rez
    # Для всех результатов запроса
    for x in rez['results']['bindings'] :
        if x['o']['value'].isdecimal: sys.stderr.write('Warning! Не рекомендуется использовать простой_компаратор для количественных величин. Посмотрите в сторону округляющий_до_целых_компаратор и т.п.\n')
        print ( gen_subj(x) + 
            ' :имеет_особенность ' + 
            gen_feat(x)+' .'
            )  #.keys()"
    



query='''
WHERE {
?s ?p ?o .
?p :связан_с_компаратором :относительный_компаратор .
?s (:аффилирован*/:масштаб) ?masht
FILTER(isURI(?s)) .
}
'''

# Добавить условие, что объект это литерал

def gen_subj(x):
    return '<'+x['s']['value'].encode('utf-8')+'>'

def strip_obj(x):
    return str(float(x['o']['value'])/float(x['masht']['value'])).encode('utf-8')

def gen_feat(x):
    return ('<'+x['p']['value'].encode('utf-8') +
           '_отн_' +
           strip_obj(x)+'>')


    


test_query()

make()

