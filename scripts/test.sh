#!/bin/sh

# Конфигурационный файл загружается, если еще не загружен
if [ -z "$REPCAE_PATH" ] 
    then
    . "`dirname "$0"`/../config"
    fi

# относительный_компаратор

"$REPCAE_PATH/scripts/comparators/относительный_компаратор.py"
    
exit

# Тестирование объединения запросов

echo ===== $1 =====
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "PREFIX : <http://piston-engines.ru/ontologies/cae#> SELECT ?feat WHERE { {:$1  :имеет_особенность ?feat} UNION {:$1 ?pred ?cont . ?cont :имеет_особенность ?feat} }"


echo ===== $2 =====
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "PREFIX : <http://piston-engines.ru/ontologies/cae#> SELECT ?feat WHERE { {:$2  :имеет_особенность ?feat} UNION {:$2 ?pred ?cont . ?cont :имеет_особенность ?feat} }"

# Тестирование вычитания запросов


echo ===== $1 - $2 =====
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "PREFIX : <http://piston-engines.ru/ontologies/cae#> SELECT ?feat WHERE { {{:$1  :имеет_особенность ?feat} UNION {:$1 ?pred ?cont . ?cont :имеет_особенность ?feat}}MINUS{{:$2  :имеет_особенность ?feat} UNION {:$2 ?pred2 ?cont2 . ?cont2 :имеет_особенность ?feat}} }"
