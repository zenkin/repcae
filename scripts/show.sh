#!/bin/sh

# Вывод в консоль всех данных по конкретному расчету

# Конфигурационный файл загружается, если еще не загружен
if [ -z "$REPCAE_PATH" ] 
    then
    . "`dirname "$0"`/../config"
    fi

# Анализ 1 ключевого слова
case "$1" in

ттл | ttl) 
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
PREFIX owl: <http://www.w3.org/2002/07/owl#> \
DESCRIBE :$2 " ;;

все | all) 
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
SELECT * \
WHERE { ?subject a :$2 }" ;;

*) 

echo ===== $1 =====
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
PREFIX owl: <http://www.w3.org/2002/07/owl#> \
SELECT  ?predicate ?object ?p2 ?o2 \
WHERE { :$1  ?predicate ?object. \
        OPTIONAL {?object ?p2 ?o2} }"

$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
PREFIX owl: <http://www.w3.org/2002/07/owl#> \
SELECT ?subject ?predicate \
WHERE { ?subject ?predicate :$1 }"

esac
