#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import subprocess
import sys
import json
import os

FUSEKI_PATH = os.environ["FUSEKI_PATH"]
GRAPH_MAIN_QUERY = os.environ["GRAPH_MAIN_QUERY"]


# Подготовка шапки графа
print '''digraph G {
graph [layout=sfdp]
node[shape=\"note\", fontsize=10];
edge[fontsize=5];'''

# Генерация перечня расчетов для анализа
p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
                     PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                     SELECT DISTINCT *
                     WHERE {'''+GRAPH_MAIN_QUERY+'''}
                     " ''', shell=True, stdout=subprocess.PIPE)
rez = json.load(p.stdout)

base=[]
# Сборка данных для каждого расчета в базу
for x in rez['results']['bindings'] :
    rec={}
    rec['name']=x['s']['value'].split('#')[1].encode('utf-8')
    rec['label']=rec['name']
    if 'rez1' in x: rec['label']+='\\n'+x['rez1']['value'].encode('utf-8')
    else: rec['label']+='\\n-'
    if 'rez2' in x: rec['label']+='\\n'+x['rez2']['value'].encode('utf-8')
    # Запрос для каждого расчета списка его особенностей
    p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
                     PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                     SELECT DISTINCT ?f
                     WHERE { :'''+rec['name']+''' :имеет_особенность ?f }
                     " ''', shell=True, stdout=subprocess.PIPE)
    feat = json.load(p.stdout)
    # Сборка их в нужную форму
    rec['features']=[]
    for f in feat['results']['bindings'] :
        rec['features'].append(f['f']['value'].split('#')[1].encode('utf-8'))

    base.append(rec)

# Вывод списка узлов
for x in base:
    print x['name']+' [label="'+x['label']+'"]'+' ;'

color=0
# Для каждых двух расчетов
for i in range(len(base)-1):
    for j in range(i+1,len(base)):
        # Находим множества уникальных особенностей
        only1=[]
        only2=list(base[j]['features'])
        for fi in base[i]['features']:
            founded=False
            for fj in only2:
                if fi==fj: 
                    founded=True
                    only2.remove(fj)
                    break
            if not founded: only1.append(fi)
        # Проверка на близость
        if (len(only1)<2) & (len(only2)<2):
            taillabel=''
            for st in only1: taillabel+=st+'\\n'
            headlabel=''
            for st in only2: headlabel+=st+'\\n'
            color+=0.1
            if color>1: color = color-1+0.03
            print base[i]['name']+' -> '+ base[j]['name'] +' [taillabel="'+taillabel+'", headlabel="'+headlabel+'", color="'+str(color)+',1,1", fontcolor="'+str(color)+',1,1"] ;'


print '}'
