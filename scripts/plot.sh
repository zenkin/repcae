#!/bin/sh

# Построение графика с помощью гнуплот (файл анализа)

# Конфигурационный файл загружается, если еще не загружен
if [ -z "$REPCAE_PATH" ] 
    then
    . "`dirname "$0"`/../config"
    fi
    
# Проверка на пустой вызов
if [ $# -eq 0 ]
  then
    echo "Ошибка: отсутствует sparql-запрос."
    echo "Для построения графика введите в коммандной строке основную часть (WHERE) sparql-запроса без фигурных скобок. В запросе должны обязательно фигурировать переменные ?x, ?y (и ?z в случае трехмерного графика)."
    echo "Пример корректного запроса:"
    echo "реп график ?calc :посчитан_для_сетки/:построена_из_геометрии/:содержит/:условный_диаметр ?x . ?calc :Массовый_расход ?y"
    exit
fi

# Проверка на наличие требуемых переменных в запросе
if echo "$@" | grep -i -c "?x"
    then
    export xlabel=`echo $@ | sed 's/^.*[: \/]\(.*\) ?x.*$/\1/'`
    echo "x: ""$xlabel"
    else
    echo "Ошибка: в sparql-запросе отсутствует переменная ?x."
    exit
fi

if echo "$@" | grep -i -c "?y"
    then 
    export ylabel=`echo $@ | sed 's/^.*[: \/]\(.*\) ?y.*$/\1/'`
    echo "y: ""$ylabel"
    else
    echo "Ошибка: в sparql-запросе отсутствует переменная ?y."
    exit
fi

if echo "$@" | grep -i -c "?z"
    then 
    export zlabel=`echo $@ | sed 's/^.*[: \/]\(.*\) ?z.*$/\1/'`
    echo "z: ""$zlabel"
    "$REPCAE_PATH/scripts/plot3d.sh" $@
    else
    "$REPCAE_PATH/scripts/plot2d.sh" $@
    exit
fi
