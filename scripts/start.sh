#!/bin/sh

# Начало работы с репозиторием

# Конфигурационный файл загружается, если еще не загружен
if [ -z "$REPCAE_PATH" ] 
    then
    . "`dirname "$0"`/../config"
    fi

# Проверка, что сервер не загружен ранее
if curl http://localhost:3030/$/ping 
    then
    echo "Сервер уже загружен!"
    exit
    fi

# Запуск сервера    
nohup "$REPCAE_PATH/scripts/runserver.sh" >/dev/null 2>"$REPCAE_PATH/server.log" &

# Ожидание сервера
sleep 2
for a in 1 2 3 4 5
do
if curl http://localhost:3030/$/ping 
    then
    # Перечень команд, загружаемых после старта сервера:
    "$REPCAE_PATH/scripts/load_repository.sh"
    "$REPCAE_PATH/scripts/comparator.sh"
    exit
    else
    sleep $a
    fi
done

echo "Не могу загрузить сервер фузеки!"
