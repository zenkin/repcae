#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import subprocess
import sys
import json
import os

# Через параметры командной строки передаются расчеты для анализа
calc1 = sys.argv[1]
calc2 = sys.argv[2]

# FUSEKI_PATH="/home/vladimir/soft/apache-jena-fuseki-3.8.0/"
FUSEKI_PATH = os.environ["FUSEKI_PATH"]

# Запрос для первого расчета списка его особенностей
p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
                PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                SELECT DISTINCT ?f
                WHERE { :'''+calc1+''' :имеет_особенность ?f }
                " ''', shell=True, stdout=subprocess.PIPE)
feat = json.load(p.stdout)
# Сборка их в нужную форму
calc1_feats=[]
for f in feat['results']['bindings'] :
    calc1_feats.append(f['f']['value'].split('#')[1])


# Запрос для второго расчета списка его особенностей
p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
                PREFIX : <http://piston-engines.ru/ontologies/cae#> 
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                SELECT DISTINCT ?f
                WHERE { :'''+calc2+''' :имеет_особенность ?f }
                " ''', shell=True, stdout=subprocess.PIPE)
feat = json.load(p.stdout)
# Сборка их в нужную форму
calc2_feats=[]
for f in feat['results']['bindings'] :
    calc2_feats.append(f['f']['value'].split('#')[1])


# Находим множества уникальных особенностей
feats_12=[] # Это список общих особенностей
calc1_feats_=[] # Это уникальные особенности 1 расчета. Для второго все в исходном массиве
for f1 in calc1_feats:
    founded=False
    for f2 in calc2_feats:
        if f1==f2: 
            founded=True
            #calc1_feats.remove(f1)
            calc2_feats.remove(f2)
            feats_12.append(f1)
            break
    if not founded: calc1_feats_.append(f1)

# Генерация хтмл
print '''
<html>
<meta charset="UTF-8"/>
<head>
<title>Сравнение расчетов</title>
</head>
<body>
<table border="1" cellpadding="12">
<tr>
<td>Наименование расчета</td>
<td>Уникальные особенности</td>
<td>Общие особенности</td>
</tr>
<tr>
<td>'''+calc1+'''</td>
<td>'''
for f in calc1_feats_: print "<li>"; print f.encode('utf-8')
print '''</td>
<td rowspan="2">'''
for f in feats_12: print "<li>"; print f.encode('utf-8')
print '''</td>
</tr>
<tr>
<td>'''+calc2+'''</td>
<td>'''
for f in calc2_feats: print "<li>"; print f.encode('utf-8')
print '''</td>
</tr>
</table>
</body>
</html>
'''



