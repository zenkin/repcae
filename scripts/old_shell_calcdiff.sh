# УСТАРЕЛО!!! Файл потенциально рабочий, но актуальнее использовать версию на питоне
# Скрипт создает граф мало отличающихся друг от друга расчетов
# 
# Псевдокод
# 
# Сформировать папку с файлами - по одному файлу на каждый интересующий расчет
# Повторять для каждой уникальной пары файлов
#       Посчитать количество уникальных меток в 1 и 2 файле
#       Если число мало - добавить соответствующую пару в дот-файл
# Отобразить дот-файл

. './config'
DOTFILE="tmp/conn.dot"

# Очистка каталога с временными файлами
rm "$REPCAE_TEMP"/*

# Подготовка шапки графа
echo "digraph G {" > $DOTFILE
echo "graph [layout=circo]" >> $DOTFILE
echo "node[shape=\"note\", fontsize=10];" >> $DOTFILE
echo "edge[fontsize=5];" >> $DOTFILE

# Генерация перечня расчетов для анализа
CALCS=(`$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=csv "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
SELECT DISTINCT ?s \
WHERE { ?s  rdf:type :Расчет }"`)


for ((i=1; i <= ${#CALCS[@]}-1 ; i++))
    do
    #echo ${CALCS[$i]}
    
    # Экстрагируем имена для расчетов
    # Проблема: тут вылазит какой-то непечатный символ в конце, надо бы его убить, но пока не пойму как
    filename[$i]=`echo ${CALCS[$i]} | cut -d# -f2`
    
    # Каждый расчет представляем в графе как узел
    echo ${filename[$i]} >> $DOTFILE
    
    # Для каждого расчета выгружаем в файл его особенности
    $FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=csv "\
    PREFIX : <http://piston-engines.ru/ontologies/cae#> \
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
    SELECT DISTINCT ?f \
    WHERE { :${filename[$i]}  :имеет_особенность ?f }" \
    | cut -d# -f2 \
    | sort \
    > tmp/${filename[$i]}
    
    done

# Функция возвращает 1, если два расчета считаются близко связанными
checkdiff()
{
if [[ "$1" -lt "2" && "$2" -lt "2" ]]
   then
   return 1
   else
   return 0
   fi
}    

# Двойной перебор по всем расчетам
for ((i=1; i <= ${#filename[@]}-1 ; i++))
    do
    for ((j=i+1; j <= ${#filename[@]} ; j++))
        do
        checkdiff `comm -123 --total tmp/${filename[i]} tmp/${filename[j]}`
        if [ "$?" -eq "1" ]
            then
            echo -n "${filename[i]} -> ${filename[j]} [label=\"" >> $DOTFILE
            echo "+`comm -23 tmp/${filename[i]} tmp/${filename[j]}`\\n\
            -`comm -13 tmp/${filename[i]} tmp/${filename[j]}`\"]" >> $DOTFILE
            fi
        done
    
    done



# Закрытие файла графа
echo "}" >> $DOTFILE


# Открываем файл графа в программе
nohup xdot $DOTFILE $@ >/dev/null 2>/dev/null &
