#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Скрипт интерактивного добавления новых объектов в репозиторий. 

import subprocess
import sys
import json
import os

#FUSEKI_PATH = os.environ["FUSEKI_PATH"]

# Метка состояния в графе конечного автомата
ST_START = 1
ST_WAITSUBJ = 1
ST_WAITPROP = 2
ST_WAITOBJ = 3
state = ST_START

# Список для сбора данных под запись
newsubj=[]

#if len(sys.argv)<2:
#    print 'Необходимо передать название рассматриваемой особенности в командной строке!'


def printhelp():
    print "Тут будет выводиться помощь."





# =================================================================
# Состояние: ожидает ввода названия объекта
def waitsubj(st):
    print 'Введите наименование объекта:'
    # Проверить на корректность

    # Проверить на наличие в базе

    countwords=len(st.split())
    if countwords:
        print 'Введите команду или наименование субъекта.'
        state = ST_WAITSUBJ
    elif countwords==0:
        newsubj.append(st.split[0])
        state = ST_WAITPROP
    elif countwords==1:
        newsubj.append(st.split[0])
        newsubj.append('    '+st.split[1])
        state = ST_WAITOBJ
    elif countwords==2:
        newsubj.append(st.split[0])
        newsubj.append('    '+st.split[1])
        newsubj.append('        '+st.split[2])
        if st.split[2][-1]==',':
            state = ST_WAITOBJ
        elif st.split[2][-1]==';':
            state = ST_WAITPROP
    else:
        print 'Error'

def waitprop(st):
    print 'Введите наименование свойства:'
    # Проверить на корректность

    # Проверить на наличие в базе

    if len(st.split)==0:
        print 'Введите команду или наименование субъекта.'
    elif len(st.split)==1: 
        newsubj.append('    '+st.split[0])
        state = ST_WAITOBJ
    elif len(st.split)==2: 
        newsubj.append('    '+st.split[0])
        newsubj.append('        '+st.split[1])
        if st.split[2][-1]==',':
            state = ST_WAITOBJ
        elif st.split[2][-1]==';':
            state = ST_WAITPROP
    else:
        print 'Error'

def waitobj(st):
    print 'Введите наименование объекта:'
    # Проверить на корректность

    # Проверить на наличие в базе

    if len(st.split)==0:
        print 'Введите команду или наименование.'
    elif len(st.split)==1: 
        newsubj.append('        '+st.split[0])
        if st.split[2][-1]==',':
            state = ST_WAITOBJ
        elif st.split[2][-1]==';':
            state = ST_WAITPROP
    else:
        print 'Error'


print "Вы находитесь в интерактивной оболочке ввода объектов."
print "Для получения короткой справки наберите <помощь> или <help>"
print "Для выхода без изменений наберите <выход>, или <exit>, или <end>."
print "Завершение ввода объекта - символ точки <.>"


print '----------------------------------------'

# Цикл ввода
while True:
    st=raw_input('> ')

    if st=='end':
        exit()
    elif st=='help':
        printhelp()
    elif st=='list':
        print newsubj
    elif st=='.':
        # Тут должна быть запись в файл
        print newsubj
        exit()
    else:
        if state==ST_WAITSUBJ:
            waitsubj(st)
        elif state==ST_WAITPROP:
            waitprop(st)
        elif state==ST_WAITOBJ:
            waitobj(st)





# Перечень расчетов, содержащих особенность
#p = subprocess.Popen(FUSEKI_PATH+'''/bin/s-query --service=http://localhost:3030/cae --output=json "
#                     PREFIX : <http://piston-engines.ru/ontologies/cae#> 
#                     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
#                     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
#                     SELECT DISTINCT *
#                     WHERE {?s  rdf:type :Расчет. ?s :имеет_особенность '''+ test_feature +'''}
#                     " ''', shell=True, stdout=subprocess.PIPE)
#rez = json.load(p.stdout)
