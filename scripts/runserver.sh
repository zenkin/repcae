#!/bin/sh

# Скрипт запускает сервер в режиме файла с аплоадом

# Конфигурационный файл загружается, если еще не загружен
if [ -z "$REPCAE_PATH" ] 
    then
    . "`dirname "$0"`/../config"
    fi

cd $FUSEKI_PATH
./fuseki-server --config="$REPCAE_PATH/fuseki-config.ttl"


# Самый простой вариант запуска
# ./fuseki-server --update --mem /cae
