#!/bin/sh

# Построение трехмерного графика с помощью гнуплот

# Конфигурационный файл загружается, если еще не загружен
if [ -z "$REPCAE_PATH" ] 
    then
    . "`dirname "$0"`/../config"
    fi

#REQ="?calc rdf:type :Расчет. \
#?calc :посчитан_для_сетки/:построена_из_геометрии/:Выходное_сечение_диаметр ?x . \
#?calc :посчитан_для_сетки/:построена_из_геометрии/:Промежуточное_сечение_условный_диаметр ?y . \
#?calc :Массовый_расход ?z"

REQ=$@

$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=csv "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
SELECT  ?x ?y ?z \
WHERE { $REQ }"


gnuplot -persist <<-EOFMarker
    set grid
    unset key
    set style line 1 lc rgb 'black' pt 7
    set xlabel "$xlabel"
    set ylabel "$ylabel"
    set zlabel "$zlabel"
    set datafile separator ","
    set terminal wxt enhanced
    splot "-" using 1:2:3 with points ls 1
`$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=csv "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
SELECT  ?x ?y ?z \
WHERE { $REQ }"`
EOFMarker
