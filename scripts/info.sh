#!/bin/sh

# Статистика репозитория

# Конфигурационный файл загружается, если еще не загружен
if [ -z "$REPCAE_PATH" ] 
    then
    . "`dirname "$0"`/../config"
    fi

echo ===== Статистика репозитория =====
echo ==================================

echo ===== Перечень расчетов
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
PREFIX owl: <http://www.w3.org/2002/07/owl#> \
SELECT DISTINCT ?s \
WHERE { ?s  rdf:type :Расчет }"

echo ===== Перечень сеток
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
PREFIX owl: <http://www.w3.org/2002/07/owl#> \
SELECT DISTINCT ?s \
WHERE { ?s  rdf:type :Сетка }"

echo ===== Перечень геометрий
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
PREFIX owl: <http://www.w3.org/2002/07/owl#> \
SELECT DISTINCT  ?s \
WHERE { ?s  rdf:type :Геометрия }"

echo ===== Перечень прочих элементов
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
PREFIX owl: <http://www.w3.org/2002/07/owl#> \
SELECT DISTINCT  ?s ?type \
WHERE { ?s ?p ?o . \
OPTIONAL{?s rdf:type ?type}}" | sed -e '/:Расчет/d' -e '/:Сетка/d' -e '/:Геометрия/d'

echo ===== Перечень искусственно сгенерированных особенностей
$FUSEKI_PATH/bin/s-query --service=http://localhost:3030/cae --output=text "\
PREFIX : <http://piston-engines.ru/ontologies/cae#> \
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
PREFIX owl: <http://www.w3.org/2002/07/owl#> \
SELECT DISTINCT ?o \
WHERE { ?s  :имеет_особенность ?o } \
"


